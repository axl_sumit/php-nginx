# Use an official PHP 8.1 image as the base image
FROM php:8.1-fpm

# Set environment variables
ENV PHP_OPCACHE_ENABLE=1
ENV PHP_OPCACHE_ENABLE_CLI=0
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS=0
ENV PHP_OPCACHE_REVALIDATE_FREQ=0
ENV PHP_OPCACHE_MEMORY_CONSUMPTION=256


ENV PHP_MAX_EXECUTION_TIME=600
ENV PHP_MEMORY_LIMIT=800M
ENV PHP_DISPLAY_ERRORS=Off
ENV PHP_POST_MAX_SIZE=150M
ENV PHP_UPLOAD_MAX_FILESIZE=150M
ENV PHP_MAX_FILE_UPLOADS=20



# Configure PHP-FPM
COPY ./php/php.ini /user/local/etc/php/php.ini
# COPY ./php/php-fpm.conf /usr/local/etc/php-fpm.d/php-fpm.conf
COPY ./php/php-fpm.conf /usr/local/etc/php-fpm.d/www.conf


# Install PHP extensions
RUN apt-get update -yqq \
# Install libs for building PHP exts
    && apt-get install -yqq --no-install-recommends \
        zip \
        libzip-dev \
        libicu-dev \
        libpq-dev \
        libmcrypt-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        unzip \
        nano \ 
        supervisor \  
    && pecl install redis-5.3.7 \
    # Install PHP Exts
    && docker-php-ext-install \
        pdo_mysql \
        pdo_pgsql \
        pgsql \
        mysqli \
        zip \
        opcache \
        ctype \
        exif \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable redis \
    # Remove dev packages
    && apt-get remove --purge -yyq libicu-dev \
        libpq-dev \
        libmcrypt-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
    && rm -r /var/lib/apt/lists/*

# Install Supervisor
RUN rm -rf /var/lib/apt/lists/*
# install composer and dependencies for composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && HASH=`curl -sS https://composer.github.io/installer.sig` \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer


#Install Nginx webserver and clean up the package lists
RUN apt-get update && apt-get install -y \
    nginx 


# Configure Nginx
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# Create a directory for PHP session files
RUN mkdir -p /var/lib/php/sessions && chown -R www-data:www-data /var/lib/php/sessions

# Reload PHP-FPM to apply OpCache changes
RUN kill -USR2 1

# Start both Nginx and PHP-FPM
CMD ["nginx", "-g", "daemon off;"]

